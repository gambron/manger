MaNGeR   (Massively parallel Numerical General relativity and Reheating)
https://gitlab.com/gambron/manger

Copyright (C) 2019 Philippe Gambron

Distributed under the GNU General Public License version 3 (GPLv3.txt or https://www.gnu.org/licenses/gpl-3.0.html)

